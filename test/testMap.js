const elements = require('./arrayData');
const map = require('../map');

let result = map(elements, function cb(element, index, elements) {
    return element * 2;
});

console.log(result);