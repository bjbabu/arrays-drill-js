const elements = require('./arrayData');
const find = require('../find');

const res = find(elements, function cb(element, index){
    return element%2 == 0;
});

console.log(res);