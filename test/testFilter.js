const elements = require('./arrayData');
const filter = require('../filter');

const result = filter(elements, function cb(element, index) {
    return element % 2 == 0;
});

console.log(result);