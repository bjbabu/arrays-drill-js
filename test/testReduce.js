const elements = require('./arrayData');
const reduce = require('../reduce');

const result = reduce(elements, function cb(startingValue, element, index, elements) {
    return startingValue + element;
}, 0);

console.log(result);