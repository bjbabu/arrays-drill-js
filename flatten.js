
function flatten(elements) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

    let result = [];

    function recurFlat(element){
        for(let index = 0; index < element.length; index++) {

            if(!Array.isArray(element[index])){
    
                result.push(element[index]);
    
            }else{
    
                recurFlat(element[index]);
    
            }
        }
    }
    recurFlat(elements);

    return result;
}


module.exports = flatten;